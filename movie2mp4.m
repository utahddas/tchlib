function movie2mp4(M,filename)
% movie2mp4 - covert Matlab movie to mp4 and write to file
% On input:
%     M (Matlab ,movie): movie to be saved as avi file
%     filename (string): name of mp4 file (mp4 extension default)
% On output:
%     <file written>: filename.mp4
% Call:
%     movie2mp4(M,'track');
% Author:
%     Taylor Welker -- modified by Tom Henderson
%     UU
%     Spring 2019
%

vidObj = VideoWriter(filename,'MPEG-4');
open(vidObj);
writeVideo(vidObj,M);
close(vidObj);
