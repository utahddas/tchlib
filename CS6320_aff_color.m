function A = CS6320_aff_color(im)
% CS6320_aff_color - make affinity array based on color values
% On input:
%     im (mxnx3 image): color image
% On output:
%     A (mnxmn array): affinity array (pixel-wise)
% Call:
%     A = CS6320_aff_color(im);
% Author:
%     T. Henderson
%     UU
%     Spring 2014
%

sigma = 1;

[num_rows,num_cols,num_planes] = size(im);
num_el = num_rows*num_cols;
im1 = reshape(im,num_rows*num_cols,3);
im1 = double(im1);
A = zeros(num_rows,num_cols);
h = waitbar(0,'aff-color');
for index1 = 1:num_el-1
    waitbar(index1/num_el);
    v1 = im1(index1,1:3);
    for index2 = index1+1:num_el
        v2 = im1(index2,1:3);
%        A(index1,index2) = exp(-(norm(v1-v2)^2/(2*sigma^2)));
        if norm(v1-v2)==0
            A(index1,index2) = 1;
        else
            A(index1,index2) = 1/norm(v1-v2);
        end
        A(index2,index1) = A(index1,index2);
    end
end
close(h);
