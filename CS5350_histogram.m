function [H,bin_bounds] = CS5350_histogram(vals,num_bins)
% CS5350_histogram - histogram values into bins
% On input:
%     vals (vector): values to histogram
%     num_bins (int): number of bins for histogram
% On output:
%     H (1xnum_bins vector): histogram
%     bin_bounds (): bounds on each bin
% Call:
%     [H,bb] = CS5350_histogram([1:200],10);
% Author:
%     T. Henderson
%     UU
%     Fall 2013
%

H = zeros(num_bins,1);
if isempty(vals)
    H(1) = 1;
    bin_bounds = ones(num_bins+1,1);
    return
end

max_val = max(vals);
min_val = min(vals);
spread = max_val - min_val;
num_vals = length(vals);

% if 0-length interval then return all in first bin
if spread == 0
    H(1) = num_vals;
    bin_bounds = ceil(max_val)*ones(num_bins+1,1);
    return
end

del_x = spread/num_bins;
bin_bounds = [min_val:del_x:max_val];
bin_bounds(1) = floor(bin_bounds(1));
bin_bounds(end) = ceil(bin_bounds(end));

for n = 1:num_vals
    v = vals(n);
    for b = 2:num_bins+1
        if v<=bin_bounds(b)
            H(b-1) = H(b-1) + 1;
            break
        end
    end
end

