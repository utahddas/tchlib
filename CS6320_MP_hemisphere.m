function mp = CS6320_MP_hemisphere(radius,x_min,x_max,dx,y_min,y_max,dy)
% CS6320_MP_hemisphere - make range image of hemisphere
% On input:
%     radius (float): radius of cylinder
%     x_min (float): min x value
%     x_max (float): max x value
%     dx (float): step in x
%     y_min (float): min in y
%     y_max (float): max in y
%     dy (float): step in y
% On output:
%     mp (mxnx3 array): XYZ image of hemisphere
% Call:
%     mp_hemi = CS6320_MP_hemi(10,-10.1,10.1,1,-10.1,10.1,1);
% Author:
%     T. Henderson
%     UU
%     Spring 2014
%

x_vals = [x_min:dx:x_max];
y_vals = [y_min:dy:y_max];
num_x_vals = length(x_vals);
num_y_vals = length(y_vals);
radius_sq = radius^2;
mp = zeros(num_y_vals,num_x_vals,3);

for x_ind = 1:num_x_vals
    x = x_vals(x_ind);
    for y_ind = 1:num_y_vals
        y = y_vals(y_ind);
        if sqrt(x^2+y^2)<=radius^2
            d2 = max(0,radius_sq-x^2-y^2);
            mp(y_ind,x_ind,1) = x;
            mp(y_ind,x_ind,2) = y;
            mp(y_ind,x_ind,3) = sqrt(d2);
        end
    end
end
