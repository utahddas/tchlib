function [c,n] = clsq(A,dim)
%
[m,p] = size(A);
if p < dim+1, disp('Not enough unknowns');end;
if m<dim, disp('Not enough equations');end;
m = min(m,p);
R = triu(qr(A));
[U,S,V] = svd(R(p-dim+1:m,p-dim+1:p));
n = V(:,dim);
c = -R(1:p-dim,1:p-dim)\R(1:p-dim,p-dim+1:p)*n;
