function mp = CS6320_MP_cyl(radius,x_min,x_max,dx,dy)
% CS6320_MP_cyl - make range image of cylinder
% On input:
%     radius (float): radius of cylinder
%     x_min (float): min x value
%     x_max (float): max x value
%     dx (float): step in x
%     dy (float): step in y
% On output:
%     mp (mxnx3 array): XYZ image of cylnder
% Call:
%     mp_cyl = CS6320_MP_cyl(10,-10.1,10.1,.1,.1);
% Author:
%     T. Henderson
%     UU
%     Spring 2014
%

y_min = -radius-1;
y_max = radius+1;
x_vals = [x_min:dx:x_max];
y_vals = [y_min:dy:y_max];
num_x_vals = length(x_vals);
num_y_vals = length(y_vals);
radius_sq = radius^2;
mp = zeros(num_y_vals,num_x_vals,3);

for x_ind = 1:num_x_vals
    x = x_vals(x_ind);
    for y_ind = 1:num_y_vals
        y = y_vals(y_ind);
        if sqrt(y^2)<=radius
            d2 = max(0,radius_sq-y^2);
            mp(y_ind,x_ind,1) = x;
            mp(y_ind,x_ind,2) = y;
            mp(y_ind,x_ind,3) = sqrt(d2);
        end
    end
end
