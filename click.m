function c = click(fig,f)
%
% click: click on figure fig prints row, col, value in the command window
% On input:
%     fig (in): figure number
%     f(mxn image): image to choose values from for clicked location
% On output:
%     c (kx3 array): clicked locations and values
% Call:
%     c = click(fig,f);
%

[m,n] = size(f);
figure(fig);
index = 0; buttun = 1; hold on;
while buttun~=2&buttun~=3
  [x,y,buttun] = ginput(1);
  index = index+1;
  xt = min(n,max(1,round(x)));
  yt = min(m,max(1,round(y)));
  plot(xt,yt,'oy');
  c(index,1) = yt;
  c(index,2) = xt;
  c(index,3) = f(yt,xt);
  disp('Clicked at: ');
  disp(c(index,:));
end
hold off
