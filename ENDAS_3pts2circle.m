function [a,b,r] = ENDAS_3pts2circle(x1,y1,x2,y2,x3,y3)
% ENDAS_3pts2circle - find circle through 3 planar points
% On input:
%     x1 (float): x coordinate of point 1
%     y1 (float): y coordinate of point 1
%     x2 (float): x coordinate of point 2
%     y2 (float): y coordinate of point 2
%     x3 (float): x coordinate of point 3
%     y3 (float): y coordinate of point 3
% On output:
%     a (float): x coordinate of center of circle
%     b (float): y coordinate of center of circle
%     r (float): radius of circle
% Call:
%     [a,b,r] = ENDAS_3pts2circle(1,0,0,1,-1,0);
% Author:
%     T. Henderson
%     UU
%     Summer 2015
%

A = 2*[(x2-x1),(y2-y1);(x3-x2),(y3-y2)];
d = [x2^2+y2^2-x1^2-y1^2;x3^2+y3^2-x2^2-y2^2];
p = A\d;
a = p(1);
b = p(2);
r = sqrt((x1-a)^2+(y1-b)^2);
