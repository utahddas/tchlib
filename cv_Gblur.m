function b = cv_Gblur(image_in,num_rows,num_cols,sigma)
% cv_Gblur - Gaussian blur an image
% On input:
%     image_in (mxn image): input image
%     num_rows (int): number of rows in blur filter
%     num_cols (int): number of cols in blur filter
%     sigma (float): sigma in pixels
% On output:
%     b (mxn image): blurred image
% Call:
%     imGb = cv_Gblur(im1,7,7,1.5);
% Author:
%     T. Henderson
%     UU
%     Spring 2014
%

h = fspecial('gaussian', [num_rows,num_cols], sigma);
b = filter2(h,image_in);
