function mp = CS6320_add_xy(imz)
% CS6320_add_xy - add x and y to depth image
% On input:
%     imz (mxn image): depth image
% On output:
%     mp (mxnx3 image): x and y are just integer indexes of pixels
% Call:
%     mp_cyl = CS6320_add_xy(cyl);
% Author:
%     T. Henderson
%     UU
%     Spring 2014
%

[num_rows,num_cols] = size(imz);
x_vals = [1:num_cols];
y_vals = [1:num_rows];
num_x_vals = length(x_vals);
num_y_vals = length(y_vals);

mp = zeros(num_y_vals,num_x_vals,3);

for y_ind = 1:num_y_vals
    y = y_vals(y_ind);
    for x_ind = 1:num_x_vals
        x = x_vals(x_ind);
        mp(y_ind,x_ind,1) = x;
        mp(y_ind,x_ind,2) = y;
        mp(y_ind,x_ind,3) = imz(y_ind,x_ind);
    end
end
