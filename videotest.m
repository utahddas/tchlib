function videotest(M)
% Taylor Welker

% Prepare the new file.
vidObj = VideoWriter('tch.avi');
open(vidObj);

% Create an animation.
%Z = peaks; surf(Z);
%axis tight manual
%set(gca,'nextplot','replacechildren');

for k = 1:10
   %surf(sin(2*pi*k/20)*Z,Z)
   frame = M(k);
   image(frame.cdata);
   % Write each frame to the file.
   currFrame = getframe(gcf);
   writeVideo(vidObj,currFrame);
end

% Close the file.
close(vidObj);
