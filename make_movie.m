function M = make_movie(y0,v0,a,del_t,max_t)
%
% makes a movie of a bouncing ball:
%  y0: initial height
%  v0: initial velocity
%  a: acceleration
%  del_t: time step
%  max_t: duration of simulation

t = 0;
y = y0;
index = 0;
v = v0;
while t<max_t
    t = t + del_t;
    index = index + 1;
    clf
    set(gca,'visible','off');
    plot(-1,-0.1,'w.');
    hold on
    plot(1,0.1,'w.');
    plot(0,y0+0.1,'w.');
    v = v + a*del_t;
    y = y + v*del_t;
    if y<0
        y = abs(y);
        v = -v;
    end
    plot(0,y,'ro');
    M(index) = getframe(gcf);
    tch = 0;
end
