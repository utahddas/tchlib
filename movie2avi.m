function movie2avi(M,filename)
% movie2avi - covert Matlab movie to avi and write to file
% On input:
%     M (Matlab ,movie): movie to be saved as avi file
%     filename (string): name of avi file (avi extension default)
% On output:
%     <file written>: filename.avi
% Call:
%     movie2avi(M,'track');
% Author:
%     Taylor Welker -- modified by Tom Henderson
%     UU
%     Spring 2019
%

vidObj = VideoWriter([filename,'.avi']);
open(vidObj);

num_frames = length(M);
for k = 1:num_frames
   frame = M(k);
   imshow(frame.cdata);
   currFrame = getframe(gcf);
   writeVideo(vidObj,currFrame);
end

close(vidObj);