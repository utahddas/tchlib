function mp = CS6320_MP_bottle(b1,b2,b3,b4,b5,b6,b7,...
    r1,r2,r3,r4,r5,r6,x_min,x_max,del_x,y_min,y_max,del_y)
% CS6320_MP_bottle - make a depth image of the bottle in Forsythe & Ponce
% On input:
%     b1 (float): first bottle demarcation
%     b2 (float): second bottle demarcation
%     b3 (float): third bottle demarcation
%     b4 (float): fourth bottle demarcation
%     b5 (float): fifth bottle demarcation
%     b6 (float): sixth bottle demarcation
%     b7 (float): seventh bottle demarcation
%     r1 (float): radius of first slice
%     r2 (float): radius of second slice
%     r3 (float): radius of third slice
%     r4 (float): radius of fourth slice
%     r5 (float): radius of fifth slice
%     r6 (float): radius of sixth slice
%     x_min (float): min value in x
%     x_max (float): max value in x
%     del_x (float): step in x
%     y_min (float): min value in y
%     y_max (float): max value in y
%     del_y (float): step in y
% On output:
%     mp (mxnx3 array): XYZ range image (2 channels)
% Call:
%     mp_bot = CS6320_MP_bottle(0,1,6,7,11,12,13,3,2,3,1,1,1.5,...
%                 -3.2,3.2,0.1,-0.2,13.2,0.1);
% Author:
%     T. Henderson
%     UU
%     Spring 2014
%

x_vals = [x_min:del_x:x_max];
y_vals = [y_min:del_y:y_max];
num_x_vals = length(x_vals);
num_y_vals = length(y_vals);

mp = zeros(num_y_vals,num_x_vals,3);

for y_ind = 1:num_y_vals
    y = y_vals(y_ind);
    if y<b2
        radius = sqrt(r1^2-(0.5-y)^2);
    elseif y<b3
        radius = r2;
    elseif y<b4
        radius = sqrt(r3^2-(((b4-b3)/2)-(y-b3))^2);
        last_r4 = radius;
    elseif y<b5
        radius = r5 + (1-(y-b4)/(b5-b4))*(last_r4-r5);
    elseif y<b6
        radius = r5;
    elseif y<b7
        radius = sqrt(r6^2-(((b7-b6)/2)-(y-b6))^2);
    end
    for x_ind = 1:num_x_vals
        x = x_vals(x_ind);
        d2 = max(0,radius^2-x^2);
        mp(y_ind,x_ind,1) = x;
        mp(y_ind,x_ind,2) = y;
        mp(y_ind,x_ind,3) = sqrt(d2);
    end
end
