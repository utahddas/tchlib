function A = CS6320_aff_gray(im)
% CS6320_aff_gray - make affinity array based on gray level values
% On input:
%     im (mxn image): gray level image
% On output:
%     A (mnxmn array): affinity array (pixel-wise)
% Call:
%     A = CS6320_aff_gray(im);
% Author:
%     T. Henderson
%     UU
%     Spring 2014
%

sigma = 1;

[num_rows,num_cols] = size(im);
num_el = num_rows*num_cols;
im1 = reshape(im,num_rows*num_cols,1);
im1 = double(im1);
A = zeros(num_rows,num_cols);
h = waitbar(0,'aff-gray');
for index1 = 1:num_el-1
    waitbar(index1/num_el);
    v1 = im1(index1);
    for index2 = index1+1:num_el
        v2 = im1(index2);
        A(index1,index2) = exp(-((v1-v2)^2/(2*sigma^2)));
%        if norm(v1-v2)==0
%            A(index1,index2) = 1;
%        else
%            A(index1,index2) = 1/norm(v1-v2);
%        end
        A(index2,index1) = A(index1,index2);
    end
end
close(h);
